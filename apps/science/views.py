# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from wioframework import utilviews as uv
from wioframework.utilviews import gen_list_view as uvgvc
from science import models
from functools import reduce

NAMED_VIEWS = dict(
    reduce(
        lambda x,
        y: x +
        uv.AUDLV2(
            y,
            **y.WIDEIO_Meta.views_kwargs),
        models.MODELS,
        []))

@uv.wideio_view()
def algo_and_questions_list(request):
    filter_expr = {}
    if 'search' in request.REQUEST:
        filter_expr = {'description__icontains': request.REQUEST['search']}

    v1 = NAMED_VIEWS['Question_list']
    l1 = v1.raw(request, _RAW=1).filter(**filter_expr)
    v2 = NAMED_VIEWS['Algorithm_list']
    l2 = v2.raw(request, _RAW=1).filter(**filter_expr)

    ALLOWED_FILTERS = ["description__icontains",
                       "author__icontains",
                       "type__icontains"
                       ]

    if ((type(l1).__name__ != "HttpResponse")):
        if ((type(l2).__name__ != "HttpResponse")):
            # rl=l1.filter(**filter_expr)|l2.filter(**filter_expr)
            import itertools
            rl = itertools.chain(l1, l2)
            hits = l1.count() + l2.count()
            fromidx = 0
            toidx = hits
            sortby = []
            return uvgvc.render_gen_list(
                None,
                request,
                rl,
                None,
                hits,
                fromidx,
                toidx,
                sortby,
                perpage=2000,
                sort_enabled=False)
        else:
            return l2
    else:
        return l1

VIEWS = NAMED_VIEWS.values() + [algo_and_questions_list, ]