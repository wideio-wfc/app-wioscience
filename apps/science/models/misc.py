# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import zipfile
import datetime
import os
import urllib
import json
import sys

from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
import wioframework.fields as models


@wideiomodel
@wideio_timestamped
class VersionedContent(models.Model):
    version = models.TextField(blank=True)
    bytes = models.TextField(blank=True)

    @classmethod
    def get_list_url(cls):
        return reverse(cls.__name__ + "_list")

    def __unicode__(self):
        return self.version + self.bytes

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only


# MODELS.append(VersionedContent)
#MODELS += get_dependent_models(VersionedContent)

# ############################################################################
# PERMISSION STRATEGY
# WORK IN PROGRESS (PRIORITY LOW / DEPRECATED)
# ############################################################################

@wideiomodel
class PermissionStrategy(models.Model):
    name = models.CharField(max_length=92)
    rights = models.CharField(max_length=92)  # detail_visibility
    expr = models.TextField(verbose_name="Expression")  # lambda expression

    def __unicode__(self):
        return self.name


@wideiomodel
class VisibilityOptions(models.Model):
    # detailed content_accebility
    detail_visibility = models.ForeignKey(
        PermissionStrategy,
        related_name="detail_visibility_rel")  # eval expr
    # is this displayed in normal search
    listing_visibility = models.ForeignKey(
        PermissionStrategy,
        related_name="listing_visibility_rel")  # eval expr
