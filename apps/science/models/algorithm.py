# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import json
import logging

from django.views.decorators.csrf import csrf_exempt
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
from wioframework.safe_env import calc
from wioframework.utils import fancy_html_to_text, fancy_html_to_html_and_images, wio_send_email

import wioframework.fields as models
from wioframework.fields import JSONField



@wideio_owned("useraccount")
@wideiomodel
class RelFollowAlgorithm(models.Model):
    ctrname_s = "followed algorithm"
    ctrname_p = "followed algorithms"
    algorithm = models.ForeignKey('Algorithm')

    @classmethod
    def get_icon(cls):
        return cls.Algorithm.get_icon()

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only



@wideio_publishable()
@wideio_history_track(
    ["_nviews"],
    length=datetime.timedelta(365 *2),
    max_update_interval=datetime.timedelta(1).total_seconds())
@wideio_followable("RelFollowAlgorithm")
@wideio_commentable()
@wideio_tagged()
@wideio_owned()
@wideio_timestamped
@wideiomodel
class Algorithm(models.Model):
    """
    An algorithm describes a software solution, generally a piece of code that replies to a problem.
    An algorithm denotes a particular branch of implementation of an algorithm.

    It does not denote a particular revision of an algorithm (unless the algorithm are meant to be really different).
    """
    ctrname_s = "algorithm"
    ctrname_p = "algorithms"

    # ------------------------------------------------------------------------------
    name = with_advanced_help_text("""+1: A good name for an algorithm, generally a mix of the name of his authors, the name of the problem-solved, and the year of release.
     -1: A bad name for algorithm is just repeating the name of the problem without giving any hint on how this implementation is different from another.
""")(models.CharField(max_length=80, db_index=True, unique=True))
    image = models.ForeignKey('references.Image', null=True, related_name="algorithms_illustrated_by")
    description = with_advanced_help_text("""This is the description of the algorithm by its author.

A good description explains the principle of the algorithm, and what the benefit of the approach taken to solve the problem. If there are some limitations it is good explicited in the description.
+1: Give a link to a PDF proving more information about the way the algorithm work.
-1: Put a direct link to a repo / explain compilation intructions / standard inputs and outputs : There are other sections for this.
      """)(models.TextField(max_length=30000))
    # ------------------------------------------------------------------------------
    # -- METADATA -------------------------------------------------------
    youtube_video = models.URLField(null=True, blank=True,help_text="A demo of video of your algorithm running")
    project_page = models.URLField(null=True, blank=True, help_text="A link to page related to this algorithm as "
                                                                    "research project")
    repository_page = models.URLField(null=True, blank=True, help_text="A link to the repository of to the package "
                                                                       "containing the code of the algorithm when this"
                                                                       "one is open source")
    other_authors = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="coauthor_of",
        blank=True,
        null=True)


    history_log = models.TextField(null=True, blank=True)

    with_advanced_help_text("""This a json that provides a configuration passed to your program when it is called.""")
    parameters = JSONField(max_length=500, null=True, blank=True) #<TODO: Change into MIMEJSON Field

    principal_application = with_advanced_help_text("""
    The principal application is the most obvious application of an algorithm. The one for which it has been thought,
     or the one for which it has been mostly used so far.
     """)(
        models.ForeignKey('Question', related_name="primary_algorithms", blank=True, null=True)
    )

    all_applications = models.ManyToManyField('Question', related_name="algorithms", blank=True, null=True)

    involves = with_advanced_help_text("""As algorithm depend on each other to work correctly, it is often that progress
     on solving one question leads to better result in solving  another question. Even if an algorithm is not using
     wide.io internally it is a good thing to link to other scientific questions so that it can be improved.""")(
     models.ManyToManyField(
        'Question',
        related_name="used_in",
        blank=True,
        null=True
    ))

    # < SAFE ALGORITHMS CAN BE PROVED NOT TO LEAK DATA OVER THE INTERNET,,, (REQUIRED ON SOME DATASETS....)
    is_safe = models.BooleanField(default=False)


    # The actual package containing the files and the software
    package = models.ForeignKey('software.Package', null=True, blank=True)
    # NATURE OF THE PACKAGE ?
    is_data_flow_algorithm = models.BooleanField(default=False)
    data_flow_model = models.TextField(null=True, blank=True)


    license_pricing_model = models.TextField(
        default="1",
        app_validators=[lambda x:float(calc(x)) != None]
    )

    def eval_license_price(self, request, n=1):
        algo_var = {'nviews': self._nviews}
        user_var = {'is_scientist': request.user.is_scientist}
        return calc(
            self.eval_license_price, {
                'algo': algo_var, 'user': user_var}
    )

    @staticmethod
    def _has_image_input(o, a):
        if (o.parameters) == None:
            return False
        return "image_input" in o.parameters

    @classmethod
    def get_list_url(cls):
        return reverse(cls.__name__ + "_list")

    def get_absolute_url(self):
        return reverse(self.__class__.__name__ + "_view", args=[self.id])

    def __unicode__(self):
        return self.name

    def can_add(self, request):
        return request.user.is_authenticated

    def can_update(self, request):
        return self.owner == request.user or request.user.is_staff

    def can_view(self, request):
        return ((request.REQUEST.get('_VIEW','')=='badge') or request.user.is_authenticated())
    
    def on_view(self,request):
        if self.owner_id:
            try:
                if ((request.REQUEST.get('_VIEW','')=='badge')):
                    self.owner.get_extended_profile().increase_kudos(1)
            except Exception,e:
                logging.error(e)
                

    def compile_package(self, request):
        if self.package:
            dc = self.package.do_compile(status="HOLD", request=request)
            di = self.package.do_install(
                status="SCHEDULED",
                request=request,
                on_complete={
                    "start_job": dc})
            # dv = self.package.do_validate_package(request=request,
            # on_complete={"start_job": di}) # TODO: add validation before
            # package install

            # self.package.validation_job_id=str(dv)
            self.package.installation_job_id = str(di)
            self.package.compilation_job_id = str(dc)
            self.package.save()

    def on_add(self, request):
        if self.principal_application and self.principal_application not in self.all_applications.all(
        ):
            self.all_applications.add(self.principal_application)
        self.image.fix()  # fix image, so now django cron job will leave it
        self.save()
        if False: #FIXME: Temporaily disabled
          if self.package is not None and self.package.status < 2:
            self.compile_package(request)

        try:
            self.parameters = json.dumps(
                json.loads(
                    self.parameters), sort_keys=True, indent=4, separators=(
                    ',', ': '))
            self.save()
        except:
            pass

    def on_update(self, request):
        if self.principal_application and self.principal_application not in self.all_applications.all(
        ):
            self.all_applications.add(self.principal_application)
            
        # IF PACKAGE HAS BEEN UPDATED:
        if False: # Temporaily disabled
          if self.package is not None and self.package.status < 2:
            self.compile_package(request)  # TODO: CHECK WHEN NECESSARY
          else:
            import aiosciweb1.backoffice.lib as debug
            debug.log_info("package already compiled")


    @staticmethod
    def can_add(request):
        return request.user.is_authenticated

    def get_all_references(self):
        return []

    class Meta:
        ordering = ["-created_at"]

    class WIDEIO_Meta:
        # NO_DRAFT=True
        mandatory_fields = ["name", "image"]
        form_fieldgroups = [
            ('Basic info',
             ['name',
              'description',
              'image',
              'package',
              'is_safe',
              'project_page',
              'repository_page']),
            ('Details', ['youtube_video', 'tags', 'principal_application']),
            ('Technical Information',
             ['parameters',
              'data_flow_model',
              'is_data_flow_algorithm'])
        ]
        sort_enabled = ["created_at", "name", "followers_count"]
        search_enabled = "name"
        icon = "icon-cogs"
        form_exclude = ['history_log', "other_authors"]
        can_t_update = ['package']
        permissions = dec.perm_for_logged_users_only
        audlv_xargs = {'list_decorators': [csrf_exempt]}

        MOCKS={
           'default':[
               {
                   'name': 'algo-0000',
                    'description': 'dummy algorithm'
               }
           ]
        }

        class Gamification:
            @staticmethod
            def publish_algorithm():
                return ["bonus", "Publish a software in a container", account_url % 'action/open_terminal']

        class Actions:

            @wideio_action(icon="icon-cogs",
                           possible=lambda o,
                           r: (o.package_id is not None and r.user.is_staff),
                           mimetype="text/html")
            def compile(self, request):
                x = self.package.do_compile(request)
                return HttpResponseRedirect("/")

            @wideio_action(icon="icon-cogs",
                           possible=lambda o,
                           r: (o.package_id is not None and r.user.is_staff),
                           mimetype="text/html")
            def install(self, request):
                x = self.package.do_install(request)
                return HttpResponseRedirect("/")

            @wideio_action(
                icon="icon-cogs",
                possible=lambda o,r: r.user.is_authenticated,
                mimetype="text/html")
            def try_online(self, request):
                return HttpResponseRedirect(self.get_view_url() +"?_VIEW=run_web")

            @wideio_action(
                icon="icon-cogs",
                possible=lambda o,
                r: r.user.is_staff,
                mimetype="text/html")
            def notify_issues(self, request):
                wio_send_email(request, consent="technical_issues",
                    subject='We are facing usability issues with algorithm %s - help other access your software' %(str(self),),
                    template='models/algorithm/email_missing_part.html',
                    context={'x': self},
                    to_users=[self.owner],
                    url_notification=None)

                #TODO: keep track of sent mail yes !
                #TODO: notify internally ? sure (should we use internal notifications to trigger emails ?)
                return "alert('%s')" % (s,)


RelFollowAlgorithm.Algorithm = Algorithm
