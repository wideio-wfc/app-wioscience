# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

# -*- encoding:utf-8 -*-
import zipfile
import datetime
import os
import urllib
import json
import sys

from django.core.urlresolvers import reverse
from django.contrib.auth.models import AbstractUser  # , User
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, RequestContext
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
import backoffice.lib as debug
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
#from wioframework.models import File
from references.models import Image, LinkReference, BibliographicReference
import wioframework.fields as models


@wideio_owned("useraccount")
@wideiomodel
class RelFollowUsecase(models.Model):
    usecase = models.ForeignKey('science.Usecase')

    @classmethod
    def get_icon(cls):
        return Usecase.get_icon()

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only


@wideio_followable("RelFollowUsecase")
@wideio_moderated()
@wideio_owned()
@wideio_timestamped
@wideiomodel
class Usecase(models.Model):
    """
    A usecase describes a case where an algorithm is used inside of a real world application.
    The purpose of usecases is to help algorithm/models suppliers to understand where they algorithms
    go.
    """
    ctrname_s = "usecase"
    ctrname_p = "usecases"

    name = models.CharField(max_length=255, db_index=True)
    short_description = models.TextField(
        max_length=1000,
        help_text="A short description of the product. What does the product do ?")
    image = models.ForeignKey(Image)
    # ------------------------------------------------------------------------------
    concept_only = models.BooleanField(default=False)
    already_on_the_market = models.BooleanField(default=True)
    usage = models.TextField(
        blank=True,
        null=True,
        max_length=10000,
        help_text="Describe the usage of the product.")
    market = models.TextField(
        blank=True,
        null=True,
        max_length=10000,
        help_text="Describe the usage of the market of the product.")
    # ------------------------------------------------------------------------------
    youtube_video = models.URLField(null=True, blank=True)
    scientific_problems = models.ManyToManyField(
        'science.Question',
        related_name="applications",
        null=True,
        blank=True)

    def on_add(self, request):
        self.image.fix()

    class Meta:
        ordering = ["name"]  # ["-created_at"]
        unique_together = ["name", "wiostate"]

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only

        form_fieldgroups = [
            ('basic_info', [
                'name', 'short_description', 'image']), ('advanced_info', [
                    'usage', 'market', 'concept_only', 'already_on_the_market']), ('details', [
                        'youtube_video', 'scientific_problems']), ]
