# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

# -*- encoding:utf-8 -*-
import zipfile
import datetime
import urllib
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
from references.models import Image
import wioframework.fields as models



SCIENTIFIC_PROBLEM_DRAFT = 0
SCIENTIFIC_PROBLEM_SUBMITED = 1
SCIENTIFIC_PROBLEM_OPENED_REGISTRATION = 2
SCIENTIFIC_PROBLEM_OPENED_DATASET = 3
SCIENTIFIC_PROBLEM_OPENED_SUBMISSION = 3
SCIENTIFIC_PROBLEM_CLOSED = 4


@wideio_owned("useraccount")
@wideiomodel
class RelFollowQuestion(models.Model):
    question = models.ForeignKey('science.Question')

    #FIXME: Scientific Problem is declared after ...
    #@classmethod
    # def get_icon(cls):
    #    return Question.get_icon()

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only




@wideio_publishable()
@wideio_history_track(
    ["_nviews"],
    length=datetime.timedelta(
        365 * 2),
    max_update_interval=datetime.timedelta(1))
@wideio_followable("RelFollowQuestion")
@wideio_moderated()
@wideio_owned()
@wideio_timestamped
@wideiomodel
class Question(models.Model):
    """
    A scientific problem is the formal description of the scientific question for which we are looking for an algorithm.
    """
    ctrname_s = "problem"
    ctrname_p = "problems"

    # name field is mandatory
    name = models.CharField(
        max_length=255,
        db_index=True,
        null=False,
        blank=False,
        help_text="With nouns and keywords explains succinctly what is the goal that is trying to be achieved.")

    informal_description_of_the_inputs = models.TextField(
        null=True,
        blank=True,
        max_length=2000,
        help_text="If meaningful, describe in general words the nature of the data that the algorithm has to analyse.")



    informal_description_of_the_output = models.TextField(
        max_length=2000,
        help_text="Describe what would be the expected output")
    
    #TODO: UPDATE THIS!
    #inputs = models.ManyToManyField('superglue.TypeInfo',related_name="consumer_algorithms")
    #output = models.ForeignKey('superglue.TypeInfo', null=True, related_name="supplier_algorithms")

    image = models.ForeignKey(
        Image,
        help_text="Upload a nice image that people immediately associate with the problem"
    )

    state = models.IntegerField(
        default=0, choices=(
            (0, 'Open Problem'), 
            (1, 'Problem Solved')
        )
    )  # SCIENTIFIC PROBLEM STATES

    problem_mainteners = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="scientific_problem_mainteners_rel",
        null=True,
        blank=True,
        help_text="The mainteners are the person in charge of ensuring that the algorithm works.")

    short_description=models.TextField(max_length=512)
    
    usage = with_advanced_help_text("""
      Explain where this application is being used in general.
      +1: Provide indication on market numbers
      +1: Explain what are the critical elements in these applications.
      """)(RerankableParagraphField(help_text="Explain where this application is being used in general."))
      
    usecase = with_advanced_help_text("""
      Give a specific example of usecase of a question.
      """)(RerankableParagraphField(help_text="Give an exemple of concrete usecase of this question."))
      
    success_metrics = MultiAutoCompleteField()   
      
    assumptions = MultiAutoCompleteField()
    why_it_is_difficult = MultiAutoCompleteField()
    
    description = with_advanced_help_text("""
      Explain in detail what the problem is about.
      
      +1: A good description of the problem shall start by highlighting the goal, and then specify additional assumptions made in this problem, known difficulties, technical definitions optionally required to understand the question shall come last.
      """)(RerankableParagraphField(
        help_text="description of the problem"))


    state_of_the_art = with_advanced_help_text("""
      Explain who has contributed to the problem, the approach that have been tried so far, and the success that they have encountered.
      """)(RerankableParagraphField(
        help_text="Please provide a state of the art for this problem"))

    datasets = MultiAutoCompleteField()
    
    questions = models.ManyToManyField('network.QNA',blank=True)

    # < problems that are difficult for the same reason may have common points
    
    history_facts = models.TextField(
        max_length=10000,
        blank=True,
        null=True,
        help_text="A short summary of the history of the problem")



    #assumptions = RerankableParagraphField(auto_suggest=True)  # TAG LIKE ??
    # ESSENTIAL METADATA
    is_public = models.BooleanField(default=True)

    # THERE SHALL EXIST RELATIONS IN BETWEEN PROBLEM

    # problem graph
    syno_problems = models.ManyToManyField(
        'self',
        related_name="syno_problems_rev",
        null=True,
        blank=True,
        
        symmetrical=True)
    hyper_problems = models.ManyToManyField(
        'self',
        related_name="hyper_problems_rev",
        null=True,
        blank=True,
        symmetrical=False)  # a wider version of this problem
        
    super_problems = models.ManyToManyField(
        'self',
        related_name="super_problems_rev",
        null=True,
        blank=True,
        symmetrical=False)  # a problem that requires this one to be solved

    # related_links=models.ManyToManyField(LinkReference,related_name="question_rev",through="RelQuestionRelatedLinks")
    # bibliography=models.ManyToManyField(BibliographicReference,related_name="question_rev",through="RelQuestionBilbiography")
    # industry user interested in solution to that problems
    # money generated by algorithm solving this problem
    
    
    #links = models.ManyToManyField(
        #'references.LinkReference',
        #related_name="scientific_problems",
        #null=True,
        #blank=True)
    #bibliography = models.ManyToManyField(
        #'references.BibliographicReference',
        #related_name="scientific_problems",
        #null=True,
        #blank=True)

    @staticmethod
    def can_add(request):
        return request.user.is_authenticated

    def can_update(self, request):
        return (
            self.owner == request.user) or (
            request.user.is_staff or request.user.is_superuser)

    @staticmethod
    def allow_partial_submit():
        return True

    def on_add(self, request):
        self.image.fix()
        self.owner=request.user

    def get_description(self):
        desc = ""
        from django.db.models.query import QuerySet as qs
        for d in self.description:
            if isinstance(d, qs):
                for x in d.iterator():
                    desc = desc + x.content + "<br/>"
        return desc

    def __unicode__(self):
        return self.name

    def get_all_references(self):
        res = []
#
        # c=self.__class__
        # for fn in filter(lambda n:n[0]!="_", dir(c)):
        for f in self._meta.virtual_fields:
            if hasattr(f, "get_all_references"):
                res += f.get_all_references(self)
        return res


    '''
    def get_paragraph(self):
        return self.short_description.content
    '''

    class Meta:
        ordering = ["name"]  # ["-created_at"]
        # FIXME < THAT IS CERTAINLY NECESSARY TO CREATE INDEXES
        unique_together = ["name", "wiostate"]

    class WIDEIO_Meta:
        add_enabled=True
        search_enabled = "name"
        icon = "icon-question-sign"
        form_exclude = [
            'problem_mainteners',
            'syno_problems',
            'hyper_problems',
            'super_problems',
            'state']
        sort_enabled = ['name', 'created_at']
        permissions = dec.perm_for_logged_users_only
        form_fieldgroups = [('synopsis',
                             ['name',
                              'image',
                              'is_public']),
                            ('nature',
                             ['informal_description_of_the_inputs',
                              #'inputs',
                              'informal_description_of_the_output',
                              #'output',
                              ])]

        class Actions:

            @wideio_action(
                icon="",
                possible=lambda o,
                r: r.user.is_authenticated(),
                mimetype="text/html")
            def add_algorithm(self, request):
                from science.models import Algorithm
                return HttpResponseRedirect(
                    Algorithm.get_add_url() + "?" + urllib.urlencode({'principal_application': self.id}))

            @wideio_action(icon="icon-cogs",
                           possible=lambda o,
                           r: (r.user.is_staff),
                           mimetype="text/html")
            def create_subproblem(self, request):
                return HttpResponseRedirect(
                    self.get_add_url() + "?" + urllib.urlencode({'super_problems': self.id}))

        class Gamification:
            @staticmethod
            def follow_scientific_question(user):
                account_url = user.get_base_url() + "/%s/" + user.id + '/'
                return ["bonus", "Follow scientific questions", "/direct/questions/"]
